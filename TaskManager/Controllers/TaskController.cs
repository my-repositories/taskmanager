﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskController.cs" company="TaskManager">
//   TaskManager
// </copyright>
// <summary>
//   Defines the TaskController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TaskManager.Controllers
{
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;

    using Models;

    /// <inheritdoc />
    [Route("api/[controller]")]
    public class TaskController : BaseController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskController"/> class.
        /// </summary>
        /// <param name="db">
        /// The db.
        /// </param>
        public TaskController(DAL.TaskContext db)
        {
            Db = db;
        }

        /// <summary>
        /// Gets the db.
        /// </summary>
        private DAL.TaskContext Db { get; }

        /// <summary>
        /// The add task.
        /// </summary>
        /// <param name="taskData">
        /// The task data.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPut]
        public ActionResult AddTask([FromBody] TaskModel taskData)
        {
            if (!ModelState.IsValid)
            {
                return GetValidationErrors();
            }

            // compute current timestamp
            taskData.CreatedAt = (long)(System.DateTime.UtcNow - new System.DateTime(1970, 1, 1)).TotalSeconds;

            var newTask = Db.Tasks.Add(taskData).Entity;
            Db.SaveChanges();

            return GetResponseData(newTask);
        }

        /// <summary>
        /// The get tasks.
        /// </summary>
        /// <returns>
        /// The <see cref="TaskModel"/>.
        /// </returns>
        [HttpGet]
        public ActionResult GetTasks()
        {
            return GetResponseData(Db.Tasks);
        }

        /// <summary>
        /// The get task.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Route("{id}")]
        [HttpGet]
        public ActionResult GetTask(int id)
        {
            return GetResponseData(Db.Tasks.FirstOrDefault(task => task.Id == id));
        }

        /// <summary>
        /// The remove task.
        /// </summary>
        /// <param name="taskData">
        /// The task data.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpDelete]
        public async Task<ActionResult> RemoveTask([FromBody] TaskModel taskData)
        {
            if (!ModelState.IsValid)
            {
                return GetValidationErrors();
            }

            var task = await _db.Tasks.FindAsync(taskData.id);

            if (task == null)
            {
                return HttpNotFound();
            }

            await _db.Database.ExecuteSqlCommandAsync(GetRemoveTaskQueryText(), taskData.id);

            return GetResponseData(Db.Tasks);
        }

        /// <summary>
        /// The update task.
        /// </summary>
        /// <param name="taskData">
        /// The task data.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPatch]
        public ActionResult UpdateTask([FromBody] TaskModel taskData)
        {
            if (!ModelState.IsValid)
            {
                return GetValidationErrors();
            }

            var oldModel = Db.Tasks.AsNoTracking().FirstOrDefault(task => task.Id == taskData.Id);

            if (oldModel == null)
            {
                return Json(new { error = "Task not found" });
            }

            // Статус "Завершена" может быть присвоен только после статусов "выполняется" и "Приостановлена"
            if (taskData.Status == 3)
            {
                if ((oldModel.Status == 1 || oldModel.Status == 2))
                {
                    taskData.CompletedAt = GetCurrentTimestamp();                    
                }
                else
                {
                    taskData.Status = oldModel.Status;
                }
            }

            // TODO: Update tasks recursive
            Db.Entry(taskData).State = EntityState.Modified;
            Db.SaveChanges();
            return GetResponseData(Db.Tasks);
        }
        
        private string GetRemoveTaskQueryText()
        {
            return @"
    WITH [TaskWithAllChildTasks] ([Id], [ParentId]) AS
    (
        SELECT @p0, NULL
        UNION ALL
        SELECT [T].[Id], [T].[ParentId]
        FROM [Tasks] AS [T]
        JOIN [TaskWithAllChildTasks] AS [TC] ON ([T].[ParentId] = [TC].[Id])
    )
    DELETE FROM [Tasks]
    WHERE [Id] IN (SELECT [Id] FROM [TaskWithAllChildTasks]);";
        }
    }
}
