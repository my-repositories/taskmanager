export const TASK_STATUSES = [
    'Registered',
    'Performed',
    'Suspended',
    'Completed'
];