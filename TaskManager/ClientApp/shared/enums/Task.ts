export enum TASK_STATUS {
    Registered,
    Performed,
    Suspended,
    Completed
}